/*
* Copyright (C) 2015 by  Fernando Amat
* See license.txt for full license and copyright notice.
*
* Authors: Fernando Amat
*  TGMMparserXML.h
*
*  Created on: March 22nd, 2015
*      Author: Fernando Amat
*
* \brief Implementing interface to import TGMM output from local XML files
*/

#ifndef __TGMM_PARSE_XML_H__
#define __TGMM_PARSE_XML_H__

#include <string>
#include "../TGMMparserInterface.h"



//=================================================================================================
struct IOclass_local_file
{
	std::string filename;

	IOclass_local_file(const std::string& p) : filename(p)
	{
	};

	IOclass_local_file() {};
};


//=================================================================================================

class TGMM_parse_XML:public TGMM_parse_interface<IOclass_local_file>
{
public:	
	
	//I/O method
	int read_from_TGMM_output(int TM = -1);//virtual method that needs to be implemented for every possible type of access

protected:

private:
	
};



#endif //end of __TGMM_PARSE_XML_H__