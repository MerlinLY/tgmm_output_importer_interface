/*
* Copyright (C) 2015 by  Fernando Amat
* See license.txt for full license and copyright notice.
*
* Authors: Fernando Amat
*  TGMMparseXML.cpp
*
*  Created on: March 22nd, 2015
*      Author: Fernando Amat
*
* \brief Implementing interface to import TGMM output from local XML files
*/

#include <assert.h>
#include <string>
#include "TGMMparserXML.h"
#include "xmlParser.h"
#include "svlStrUtils.h"

int TGMM_parse_XML::read_from_TGMM_output(int TM)
{
	//check if information has been setup
	if (!isSet_fileInfo)
	{
		std::cout << "ERROR: TGMM_parse_XML::read_from_TGMM_output: file info was not set before calling this function" << std::endl;
		return 1;
	}

	//check if file exists
	FILE* fid = fopen(fileInfo.filename.c_str(), "r");
	if ( fid == NULL )
	{
		std::cout << "ERROR: TGMM_parse_XML::read_from_TGMM_output: file " << fileInfo.filename<< " does not exist" << std::endl;
		return 2;
	}
	fclose(fid);

	XMLNode xMainNode = XMLNode::openFileHelper(fileInfo.filename.c_str(),"document");
	int n = xMainNode.nChildNode("GaussianMixtureModel");

	//preallocate memory
	obj_vec.resize(n);

	std::vector<unsigned long long int> vv;
	std::vector<double> dd;

	for (int ii = 0; ii < n; ii++)
	{
		XMLNode node = xMainNode.getChildNode("GaussianMixtureModel", &ii);//this function increments ii automatically
		ii--;

		XMLCSTR aux = node.getAttribute("id");
		
		assert(aux != NULL);
		parseString<unsigned long long int>(std::string(aux), vv);
		obj_vec[ii].obj_id = vv[0];
		vv.clear();		
			
		aux = node.getAttribute("m");
		assert(aux != NULL);
		parseString<double>(std::string(aux), dd);
		for (size_t jj = 0; jj< dd.size(); jj++) 
			obj_vec[ii].xyz[jj] = dd[jj];
		dd.clear();


		obj_vec[ii].vol_id = -1;//we do not have that information in the XML file
		
	}
	return 0;
}
