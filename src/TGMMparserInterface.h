/*
* Copyright (C) 2015 by  Fernando Amat
* See license.txt for full license and copyright notice.
*
* Authors: Fernando Amat
*  TGMMparseInterface.h
*
*  Created on: March 22nd, 2015
*      Author: Fernando Amat
*
* \brief Main abstract class to define interface for any TGMM importer
*/

#ifndef __TGMM_PARSE_INTERFACE_H__
#define __TGMM_PARSE_INTERFACE_H__

#include <cstdint>
#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <math.h>



//=================================================================================================
struct xpiwit_csv_fields
{
	std::int64_t obj_id;//id of each object (Unique)
	std::int64_t vol_id;//id of the volume containing the object
	float xyz[3];//coordinates of the center of the object (in voxel space)
	
	bool operator==(const xpiwit_csv_fields& p) const
	{
		if (obj_id != p.obj_id)
			return false;

		if (vol_id != p.vol_id)
			return false;

		for (int ii = 0; ii < 3; ii++)
		if (fabs(xyz[ii] - p.xyz[ii]) > 1e-3)
			return false;

		return true;
	}

	bool operator!=(const xpiwit_csv_fields& p) const{
		return !(*this == p);
	}
};


//=================================================================================================
template<class IOclass>
class TGMM_parse_interface
{
public:	

	//constructor
	TGMM_parse_interface();

	//operators
	bool operator==(const TGMM_parse_interface& p) const;
	bool operator!=(const TGMM_parse_interface& p) const {
		return !(*this == p);
	}

	//I/O methods
	int write_to_xpiwit_CSV(const std::string &filename) const;
	
	virtual int read_from_TGMM_output(int TM = -1) = 0;//virtual method that needs to be implemented for every possible type of access
	
	int read_from_xpiwit_CSV(const std::string& filename);//to be able to import CSV files writen by the interface
	
	void set_IO_path(const IOclass& p){ fileInfo = p; isSet_fileInfo = true; };


protected:
	IOclass fileInfo;//contains all the information necessary to access the file (whether is a local file or a database)
	bool isSet_fileInfo;
	std::vector<xpiwit_csv_fields> obj_vec;//stores the list of objects parsed from TGMM

private:
	
};


//=================================================================================================
template<class IOclass>
TGMM_parse_interface<IOclass>::TGMM_parse_interface()
{
	isSet_fileInfo = false;
}


//==============================================================================================
template<class IOclass>
bool TGMM_parse_interface<IOclass>::operator==(const TGMM_parse_interface& p) const
{
	if (p.obj_vec.size() != obj_vec.size())
		return false;

	size_t ii = 0;
	for (const auto& v : obj_vec)
	{
		if (v != p.obj_vec[ii++])
			return false;
	}

	return true;
}

//=================================================================================================
template<class IOclass>
int TGMM_parse_interface<IOclass>::write_to_xpiwit_CSV(const std::string& filename) const
{
	
	std::ofstream fid(filename.c_str());

	if (!fid.is_open())
	{
		std::cout << "ERROR: at write_to_xpiwit_CSV: opening file " << filename << " to save CSV values" << std::endl;
	}

	for (const auto& p : obj_vec)
	{
		//fid << p.obj_id << "," << p.vol_id << "," << int(ceil(p.xyz[0])) << "," << int(ceil(p.xyz[1])) << "," << int(ceil(p.xyz[2])) << std::endl;
		fid << p.obj_id << "," << p.vol_id << "," << p.xyz[0] << "," << p.xyz[1] << "," << p.xyz[2] << std::endl;
	}


	fid.close();

	return 0;
}

//===========================================================================================
template<class IOclass>
int TGMM_parse_interface<IOclass>::read_from_xpiwit_CSV(const std::string& filename)
{
	
	std::ifstream  fid(filename.c_str());
	if ( !fid.is_open() )
	{
		std::cout << "ERROR: at read_from_xpiwit_CSV: opening file " << filename << " to read CSV values" << std::endl;
	}

	std::string line;
	size_t count = 0;	
	obj_vec.clear();
	while (std::getline(fid, line))
	{		
		obj_vec.resize(count + 1);
		sscanf(line.c_str(), "%lld,%lld,%f,%f,%f", &(obj_vec[count].obj_id), &(obj_vec[count].vol_id), &(obj_vec[count].xyz[0]), &(obj_vec[count].xyz[1]), &(obj_vec[count].xyz[2]));		
		count++;
	}

	
	fid.close();

	return 0;
}

#endif //end of __TGMM_PARSE_INTERFACE_H__